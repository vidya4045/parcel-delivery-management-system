
<!DOCTYPE html>
<html>
<head>
  <title>Registration</title>
  <link rel="stylesheet" type="text/css" href="style1.css">
</head>
<body>
  <div class="header">
  	<h2>REGISTER:</h2>
  </div>
	
  <form method="post" action="backreg.php">
  	<div class="input-group">
  	  <label>Username</label>
  	  <input type="text" name="username">
  	</div>
	<div class="input-group">
	  <label>Student id</label>
	  <input type="text" name="roll">
	</div>
	<div class="input-group">
	  <label>Password</label>
	  <input type="password" name="password">
	</div>
	<div>
	  <label>Confirm password</label>
	  <input type="password" name="confirm password">
	</div> 
        <div class="input-group">
  	  <button type="submit" class="btn" name="reg_user">Register</button>
  	</div>
	<p>
	  Already a member?<a href="studentlogin.php">Sign in</a>
	</p>
      </form>

</body>
</html>
